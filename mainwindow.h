//出处：https://space.bilibili.com/3546635661478471
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QHash>
#include <QCheckBox>
#include <QToolButton>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
protected:
    void clearFrame();
    void outMsg(const QString &msg, bool succeed);
    bool loadImg(QLabel *lb_preview, QLabel *lb_size, int key);
    void delImg(QLabel *lb_preview, QLabel *lb_size, QToolButton *tb, int key);
    bool getFrame(QCheckBox *chk, QList<QImage> &frames, int size);
    void saveIco(const QList<QImage> &images, const QString &filePath);
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_open_clicked();

    void on_btn_save_clicked();

    void on_btn_16_clicked();

    void on_btn_32_clicked();

    void on_btn_48_clicked();

    void on_btn_64_clicked();

    void on_btn_128_clicked();

    void on_btn_256_clicked();

    void on_btn_d0_clicked();

    void on_btn_d16_clicked();

    void on_btn_d32_clicked();

    void on_btn_d48_clicked();

    void on_btn_d64_clicked();

    void on_btn_d128_clicked();

    void on_btn_d256_clicked();

    void on_chk_smooth_clicked(bool checked);

private:
    Ui::MainWindow *ui;
    QList<QByteArray *> m_frame;
    QHash<int, QImage> m_input;
    Qt::TransformationMode m_smooth;
};
#endif // MAINWINDOW_H
